A spigotmc 1.8.8 plugin for tracking the time players are on the server and kicking them after their time runs out.

Configuration

After starting the server once with the plugin enabled, edit the config.yml file with your preferred times
and whether a player should be kicked, if another player is in close proximity (default: true == always kick).

Custom commands

/timerreset playername/@all

License

This project is licensed under the MIT license

Miscellaneous

Feel free to critic my code and leave suggestions, for I am still relatively inexperienced. A condescending tone or hate
however will still not be tolerated.
