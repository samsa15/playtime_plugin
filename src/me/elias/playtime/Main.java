package me.elias.playtime;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Optional;
import java.util.logging.Level;

public final class Main extends JavaPlugin implements Listener {
    private int sessionTime;
    private int weeklyTime;
    private LocalDateTime startTime;
    private File configFile;
    private FileConfiguration config;
    private File storageFile;
    private FileConfiguration storage;
    private HashMap<String, PlayerHandling> players = new HashMap<>();
    private static final BukkitScheduler scheduler = Bukkit.getScheduler();
    private boolean kickWhenPlayerNearby;
    private boolean initiallyInvincible;


    @Override
    public void onEnable() {
        createStorageFile();
        parseConfig();
        getServer().getPluginManager().registerEvents(this, this);
        Optional<LocalDateTime> st = parseLocalDateTime(storage.getString("startTime"));
        startTime = st.orElseGet(LocalDateTime::now);
        storage.set("startTime", startTime.toString());
        getCommand("timerreset").setExecutor(new ResetCommand(this));
    }

    @Override
    public void onDisable() {
        try {
            storage.save(storageFile);
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Could not save data!");
            System.err.println("Error saving data to file. Functionality may be compromised.");
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player.getGameMode() == GameMode.SPECTATOR) {
            return;
        }
        String playerName = player.getName();
        Optional<LocalDateTime> lastTimePlayed = parseLocalDateTime(storage.getString(playerName + ".lastPlayed"));
        int lastDayPlayed, st, wt;
        if (lastTimePlayed.isPresent()) {
            lastDayPlayed = lastTimePlayed.get().getDayOfYear();
            st = LocalDateTime.now().getDayOfYear() != lastDayPlayed ? this.sessionTime : storage.getInt(playerName + ".sessionTime");
            wt = (startTime.getDayOfYear() - lastDayPlayed) / 7 == (startTime.getDayOfYear() - LocalDateTime.now().getDayOfYear()) / 7 ? storage.getInt(playerName + ".weeklyTime") : this.weeklyTime;
        } else {
            st = sessionTime;
            wt = weeklyTime;
        }
        if (wt <= 0) {
            scheduler.scheduleSyncDelayedTask(this, () -> player.kickPlayer("Your time for this week has run out!"));
            return;
        }
        if (st <= 0) {
            scheduler.scheduleSyncDelayedTask(this, () -> player.kickPlayer("Your time for today has run out!"));
            return;
        }
        players.put(playerName, new PlayerHandling(player, st, wt, this, kickWhenPlayerNearby, initiallyInvincible));
        Thread thread = new Thread(players.get(playerName));
        thread.start();
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        String playerName = player.getName();
        storage.set(playerName + ".sessionTime", players.get(playerName).getSessionTime());
        storage.set(playerName + ".weeklyTime", players.get(playerName).getWeeklyTime());
        storage.set(playerName + ".lastPlayed", LocalDateTime.now().toString());
        try {
            storage.save(storageFile);
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Could not save data!");
            System.err.println("Error saving data to file. Functionality may be compromised.");
        }
        players.remove(playerName);
    }

    private Optional<LocalDateTime> parseLocalDateTime(String date) {
        if (date == null) {
            return Optional.empty();
        }
        String[] split = date.split("-");
        String[] fin = new String[6];
        fin[0] = split[0];
        fin[1] = split[1];
        split = split[2].split(":");
        String[] split2 = split[0].split("T");
        String tmp;
        if (split.length >= 3) {
            tmp = split[2].substring(0, 1);
        } else {
            tmp = "00";
        }
        fin[2] = split2[0];
        fin[3] = split2[1];
        fin[4] = split[1];
        fin[5] = tmp;
        int[] res = new int[fin.length];
        for (int i = 0; i < res.length; ++i) {
            res[i] = Integer.parseInt(fin[i]);
        }
        return Optional.of(LocalDateTime.of(res[0], res[1], res[2], res[3], res[4], res[5]));
    }

    private void parseConfig() {
        configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            saveResource("config.yml", false);
        }
        config = YamlConfiguration.loadConfiguration(configFile);
        config.addDefault("sessionTime", 30);
        config.addDefault("weeklyTime", 120);
        config.addDefault("kickWhenPlayerNearby", false);
        config.addDefault("spawnProtection", true);
        sessionTime = config.getInt("sessionTime") * 60;
        weeklyTime = config.getInt("weeklyTime") * 60;
        kickWhenPlayerNearby = config.getBoolean("kickWhenPlayerNearby");
        initiallyInvincible = config.getBoolean("spawnProtection");
    }

    private void createStorageFile() {
        storageFile = new File(getDataFolder(), "data.yml");
        if (!storageFile.exists()) {
            saveResource("data.yml", false);
        }
        storage = YamlConfiguration.loadConfiguration(storageFile);
    }

    public int getSessionTime() {
        return sessionTime;
    }

    public int getWeeklyTime() {
        return weeklyTime;
    }

    public HashMap<String, PlayerHandling> getPlayers() {
        return players;
    }
}
