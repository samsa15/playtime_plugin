package me.elias.playtime;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

public final class PlayerHandling implements Runnable {
    private final Player player;
    private int sessionTime;
    private int weeklyTime;
    private static final BukkitScheduler scheduler = Bukkit.getScheduler();
    private final Main plugin;
    private final boolean kickWhenPlayerNearby;
    private boolean reset = false;
    private final boolean initiallyInvulnerable;
    private static final int SPAWN_PROTECTION = 5; // DO NOT SET TO LESS THAN 1!

    public PlayerHandling(Player player, int sessionTime, int weeklyTime, Main plugin, boolean kickWhenPlayerNearby, boolean initiallyInvulnerable) {
        this.sessionTime = sessionTime;
        this.weeklyTime = weeklyTime;
        this.player = player;
        this.plugin = plugin;
        this.kickWhenPlayerNearby = kickWhenPlayerNearby;
        this.initiallyInvulnerable = initiallyInvulnerable;
    }

    @Override
    public void run() {
        if (sessionTime > weeklyTime) {
            sessionTime = weeklyTime;
        }
        double st = (sessionTime / 60d) * 10;
        st = Math.round(st);
        st /= 10;
        double wt = (weeklyTime / 60d) * 10;
        wt = Math.round(wt);
        wt /= 10;
        player.sendMessage("Welcome! Your time left in this Session is " + st + "min and your remaining time this week is "
                + wt + "min. Have fun!");
        int playedTime = 0;
        int initialSessionTime = sessionTime;
        int initialWeeklyTime = weeklyTime;
        try {
            if (initiallyInvulnerable) { //Not being subtracted from session time, might want to change for your use case.
                player.setNoDamageTicks(20 * SPAWN_PROTECTION);
                scheduler.scheduleSyncDelayedTask(plugin, () -> player.setWalkSpeed(0));
                player.sendMessage("You are invincible for " + SPAWN_PROTECTION + " seconds!");
                synchronized (this) {
                    wait((SPAWN_PROTECTION - 1) * 1000);
                }
                player.sendMessage("Your invincibility ends in 1 second!");
                synchronized (this) {
                    wait(1000);
                }
                scheduler.scheduleSyncDelayedTask(plugin, () -> player.setWalkSpeed(0.2f)); //default walk speed
                player.sendMessage("You are no longer invincible!");
            }
            while (sessionTime > 0) {
                if (sessionTime % 30 == 0 && sessionTime > 30) {
                    synchronized (this) {
                        wait(30000);
                    }
                    playedTime += 30;
                } else if (sessionTime > 30) {
                    synchronized (this) {
                        wait((sessionTime * 1000L) % 30000);
                    }
                    playedTime += sessionTime % 30;
                } else if (sessionTime == 30) {
                    player.sendMessage("You will be kicked in 30 seconds.");
                    synchronized (this) {
                        wait(20000);
                    }
                    playedTime += 20;
                } else if (sessionTime == 20) {
                    synchronized (this) {
                        wait(10000);
                    }
                    playedTime += 10;
                } else if (sessionTime > 10) {
                    synchronized (this) {
                        wait((sessionTime * 1000L) % 10000);
                    }
                    playedTime += sessionTime % 10;
                } else if (sessionTime == 10) {
                    player.sendMessage("You will be kicked in 10 seconds.");
                    synchronized (this) {
                        wait(5000);
                    }
                    playedTime += 5;
                } else if (sessionTime > 5) {
                    synchronized (this) {
                        wait((sessionTime * 1000L) % 5000);
                    }
                    playedTime += sessionTime % 5;
                } else {
                    player.sendMessage("You will be kicked in " + sessionTime + " seconds.");
                    synchronized (this) {
                        wait(1000);
                    }
                    playedTime += 1;
                }
                if (reset) {
                    playedTime = 0;
                    initialSessionTime = plugin.getSessionTime();
                    weeklyTime = plugin.getWeeklyTime();
                    reset = false;
                }
                sessionTime = initialSessionTime - playedTime;
                weeklyTime = initialWeeklyTime - playedTime; //Updated regularly on order to allow players to disconnect normally.
            }
            if (!kickWhenPlayerNearby) {
                boolean playerNearby = false;
                do {
                    for (Entity e : player.getNearbyEntities(50, 30, 50)) { //Set desired proximity to player here
                        if (e instanceof Player) {
                            playerNearby = true;
                            break;
                        }
                    }
                    if (playerNearby) {
                        player.sendMessage("A player is nearby, you may not leave the server.");
                        synchronized (this) {
                            wait(10000);
                        }
                    }
                } while (playerNearby);
            }
            scheduler.scheduleSyncDelayedTask(plugin, () -> player.kickPlayer(weeklyTime == 0 ? "Your weekly time has been exhausted, come again next week!" :
                    "Your session time has been exhausted, come again tomorrow!"));

        } catch (InterruptedException e) {
            System.err.println("Error occurred in execution of playtime");
            weeklyTime = weeklyTime - playedTime;
            sessionTime = initialSessionTime - playedTime;
            player.saveData();
            scheduler.scheduleSyncDelayedTask(plugin, () -> player.kickPlayer("An Error occurred. Try reconnecting. We apologize for the inconvenience."));
        }
    }

    public int getWeeklyTime() {
        return weeklyTime;
    }

    public int getSessionTime() {
        return sessionTime;
    }

    public void reset() {
        this.reset = true;
    }
}
