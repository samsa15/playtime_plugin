package me.elias.playtime;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;

public class ResetCommand implements CommandExecutor {
    private final Main plugin;

    public ResetCommand(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String... args) {
        if (!sender.isOp()) {
            sender.sendMessage("You do not have the permission to issue this command!");
            return false;
        }
        if (args.length == 0) {
            sender.sendMessage("No player specified. Try again.");
            return false;
        }
        HashMap<String, PlayerHandling> players = plugin.getPlayers();
        PlayerHandling ph = players.get(args[0]);
        if (args[0].equalsIgnoreCase("@all")) {
            for (String s : players.keySet()) {
                ph = players.get(s);
                ph.reset();
            }
        } else {
            if (ph == null) {
                sender.sendMessage("Player does not exist.");
                return false;
            }
            ph.reset();
        }
        sender.sendMessage("The new session time for the specified player(s) is: " + ph.getSessionTime() / 60);
        sender.sendMessage("The new weekly time for the specified player(s) is: " + ph.getWeeklyTime() / 60);
        return true;
    }
}
